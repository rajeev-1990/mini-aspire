<?php
namespace App\Traits;

trait LoanCalculatorTrait{
	/**
	 * To calculate weekly emi
	 * @param int $amount
	 * @param int $loanTerm
	 * @return int $emi
	 */ 
	public function calculateWeeklyLoan($amount, $loanTerm){
		$rate =13/ 5200;
		$emi= ceil($amount * $rate / (1 - (pow(1/(1 + $rate), $loanTerm))));
        return $emi;
	}
}